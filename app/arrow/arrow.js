'use strict';

angular.module('myApp.arrow', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/arrow', {
    templateUrl: 'arrow/arrow.html',
    controller:'ArrowCtrl',
    controllerAs:'ac'
  });
}])

.controller('ArrowCtrl', ['constellationConsumer', '$http', function(constellation, $http) {
    
    var vm = this;
    vm.constellationUrl = "" ;
    vm.accessToken = "";
   
    vm.init = init;
    init();
        
    function init(){
         $http.get('properties').then(function (response) {
            vm.constellationUrl = response.data.ip;
            vm.accessToken = response.data.credential;
            constellation.initializeClient(vm.constellationUrl, vm.accessToken, "DartsDashboard");
            constellation.onConnectionStateChanged(function (change) {
            if (change.newState === $.signalR.connectionState.connected) {
                console.log("DartsDashboard connected");
            }
        });
        constellation.connect();
        });
        
        var dartboard = new Dartboard('#dartboard');
        dartboard.render();

        document.querySelector('#dartboard').addEventListener('throw', function(d) {
            var multiple;
            var value;
            console.log(d.detail);
            if(d.detail.ring == "innerSingle" || d.detail.ring == "outerSingle" || d.detail.ring == "outerBull"){
                multiple = 1
            }
            if(d.detail.ring == "double" || d.detail.ring == "innerBull"){
                multiple = 2
            }
            if(d.detail.ring == "triple"){
                multiple = 3
            }
            value = d.detail.score / multiple;
            console.log(multiple + "  " +  value);
            constellation.sendMessage({ Scope: 'Package', Args: ['DartManager'] }, 'onDartTargetTouch', [ multiple, value ]);
        });
    }
    
    
}]);